/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/UnitTests/JUnit5TestClass.java to edit this template
 */
package com.mycompany.lab03;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

/**
 *
 * @author kantinan
 */
public class OXProgramUnitTest {
    
    public OXProgramUnitTest() {
    }
    
    @BeforeAll
    public static void setUpClass() {
    }
    
    @AfterAll
    public static void tearDownClass() {
    }
    
    @BeforeEach
    public void setUp() {
    }
    
    @AfterEach
    public void tearDown() {
    }

    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    // @Test
    // public void hello() {}
    @Test
    public void testCheckwinNoPlayBy_o_output_false(){
        String[][] table = {{"-","-","-"},{"-","-","-"},{"-","-","-"}};
        String currentPlayer = "O";
        assertEquals(false, OXProgram.checkWin(table,currentPlayer));
    }
    @Test
    public void testCheckwinRow2By_o_output_turn(){
        String[][] table = {{"-","-","-"},{"O","O","O"},{"-","-","-"}};
        String currentPlayer = "O";
        assertEquals(true, OXProgram.checkWin(table,currentPlayer));
    }
    @Test
    public void testCheckwinRow1By_x_output_turn(){
        String[][] table = {{"X","X","X"},{"-","O","O"},{"-","-","-"}};
        String currentPlayer = "X";
        assertEquals(true, OXProgram.checkWin(table,currentPlayer));
    }
    @Test
    public void testCheckwinRow3By_x_output_turn(){
        String[][] table = {{"-","O","O"},{"-","-","-"},{"X","X","X"}};
        String currentPlayer = "X";
        assertEquals(true, OXProgram.checkWin(table,currentPlayer));
    }
    @Test
    public void testCheckwinCol1By_x_output_turn(){
        String[][] table = {{"X","O","X"},{"X","O","O"},{"X","-","-"}};
        String currentPlayer = "X";
        assertEquals(true, OXProgram.checkWin(table,currentPlayer));
    }
    @Test
    public void testCheckwinCol2By_o_output_turn(){
        String[][] table = {{"X","O","-"},{"X","O","X"},{"-","O","-"}};
        String currentPlayer = "O";
        assertEquals(true, OXProgram.checkWin(table,currentPlayer));
    }
    @Test
    public void testCheckwinCol3By_x_output_turn(){
        String[][] table = {{"-","O","X"},{"-","-","X"},{"X","O","X"}};
        String currentPlayer = "X";
        assertEquals(true, OXProgram.checkWin(table,currentPlayer));
    }
    @Test
    public void testCheckwinDiagonally1By_x_output_turn(){
        String[][] table = {{"X","O","X"},{"-","X","O"},{"-","O","X"}};
        String currentPlayer = "X";
        assertEquals(true, OXProgram.checkWin(table,currentPlayer));
    }
    @Test
    public void testCheckwinDiagonally2By_o_output_turn(){
        String[][] table = {{"X","O","O"},{"-","O","X"},{"O","X","X"}};
        String currentPlayer = "O";
        assertEquals(true, OXProgram.checkWin(table,currentPlayer));
    }
    @Test
    public void testCheckDraw_output_turn(){
        String[][] table = {{"X","O","O"},{"X","O","X"},{"O","X","X"}};
        String currentPlayer = "X";
        assertEquals(true, OXProgram.checkWin(table, currentPlayer));
    }
}
